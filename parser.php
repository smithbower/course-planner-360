<?php
	/**
	 * parser.php
	 *
	 * Responsible for scraping and parsing the UBC Okanagan Academic Calendar
	 * (2013/2014) website for course listings and course requirements, for
	 * a given subject (i.e. COSC). This data is fetched from the UBC website,
	 * parsed, and then exported as a JSON object that can then be used by
	 * a front-end to display a graph relationship of each course.
	 *
	 * The course listing for a given subject is a single page with a linear
	 * listing of courses with the format: 
	 * 		SUBJ_CODE (credits) Name
	 *		Course Description
	 *		Opt /i Prerequisite: [opt one_of] % COURSE_NUM, COURSE_NUM
	 *		Opt /i Corequisite: [opt one_of] COURSE_NUM, COURSE_NUM
	 */
?>
<?php
	require 'course.php';
	require 'db.php';
	//require 'HTTP/Request2.php';
	class Parser
	{
		/**
		 * Grabs the course listing page from UBC for a given program code.
		 *
		 * $code: The string program code we're searching for, i.e. Computer
		 *		  Science is COSC.
		 *
		 * returns: The HTML page from UBC listing courses for the given code.
		 */
		public static function get_courses_html($code)
		{
			//UBCO URL format is as such:
			///www.calendar.ubc.ca/okanagan/courses.cfm?go=name&code=[code]
			//Where [code] is the string code for the program, i.e. COSC

			//We use file_get_contents.. might have to change this later if
			//server does not support it.
			$url = 'http://www.calendar.ubc.ca/okanagan/courses.cfm?'.
				   'go=name&code='.$code;

			return file_get_contents($url);
		}


		/**
		 * Parses the HTML of a course listing from UBC.
		 *
		 * $html: The HTML string to parse.
		 * $code: The program code, i.e. "COSC".
		 *
		 * return: An array of Course objects.
		 */
		public static function parse_courses($html, $code)
		{
			//Note: Course listings are linear (no links to process on this
			//page), grouped together nicely, and we can use course numbers for
			//consistent naming. This means we can write a linear top-down 
			//parser that creates courses in-situ.


			/////////////////////////
			// SETUP
			/////////////////////////
			$courses = array();


			//Begin looking for courses after <h2>Course Descriptions</h2>/
			$startPos = strpos($html, "<h2>Course Descriptions</h2>");

			if ($startPos == false) return; //TODO: Proper error handling

			//Get end position by looking for the comment
			//<!-- END: UBC CONTENT -->
			$endPos = strpos($html, "<!-- END: UBC CONTENT -->");
			if ($endPos == false) $endPos = strlen($html);

			//Clear cruft so we don't have to worry about the extra markup.
			$cleanHtml = substr($html, $startPos, $endPos - $startPos);

			//Check the length - if we're tiny, then there is no course
			//data and we have a bad course listing.
			if (strlen($cleanHtml) < 400) return; //Arbitrary value.


			/////////////////////////
			// BEGIN PARSING
			/////////////////////////

			$currentPos = 0;
			$count = 0;

			//Grab the program name.
			$currentPos = strpos($cleanHtml, "<h3>") + 4;
			$endTitlePos = strpos($cleanHtml, "</h3>", $currentPos);
			$program = substr($cleanHtml, $currentPos, $endTitlePos - $currentPos);
			$currentPos = $endTitlePos;

			$currentPos = strpos($cleanHtml, "<dt>", $currentPos);
			while ($currentPos != false)
			{ 
				//Course titles have the following format.
				//<dt><a name="101"></a>COSC 101 (3) <b>Digital Citizenship</b></dt>
				$currentPos += 13; //Skip past the preamble.

				//Get course number.
				$numEndPos = strpos($cleanHtml, '"', $currentPos);
				$courseNum = substr($cleanHtml, $currentPos, 
									$numEndPos - $currentPos);
				$currentPos = $numEndPos;

				//Get course name.
				$currentPos = strpos($cleanHtml, '<b>', $currentPos) + 3;
				$nameEndPos = strpos($cleanHtml, '</b>', $currentPos);
				$courseName = substr($cleanHtml, $currentPos,
									 $nameEndPos - $currentPos);
				$currentPos = $nameEndPos;

				//Get course description.
				//Description is encapsulated with <dd></dd> tags.
				$currentPos = strpos($cleanHtml, '<dd>', $currentPos) + 4;

				//Get the <dd> closing tag.
				$ddEndPos = strpos($cleanHtml, '</dd>', $currentPos);

				//Need to check to see if there are any prereqs or coreqs.
				//Prereqs are first, then coreqs - we ignore equivs.
				$prereqStart = strpos($cleanHtml, 'Prerequisite', $currentPos);
				$coreqStart = strpos($cleanHtml, 'Corequisite', $currentPos);

				$hasPreReq = ($prereqStart > 10 && $prereqStart < $ddEndPos) ?
							 true : false;
				$hasCoReq = ($coreqStart > 10 && $coreqStart < $ddEndPos) ?
							true : false;
				if ($hasPreReq)
				{
					$descEndPos = $prereqStart - 8; //Remove formatting.
					$hasPreReq = true;
				}
				else
					if ($hasCoReq)
					{
						$descEndPos = $coreqStart - 8; //Remove formatting.
						$hasCoReq = true;
					}
					else
						$descEndPos = $ddEndPos;


				//Course description.				
				$courseDesc = substr($cleanHtml, $currentPos,
									 $descEndPos - $currentPos);

				//Check for [3-2-0] at end of description.
				//Without removing this, json-serialization does not validate
				//with jsonlint.com
				$brakPos = strpos($courseDesc, "[");
				if ($brakPos > 0)
					$courseDesc = substr($courseDesc, 0, strlen($courseDesc) - 7);

				$currentPos = $descEndPos;

				//We search for [CODE]_courseNum[term] where term is non num or
				//letter.
				//Must do this because not every code is just 3 digits, i.e.
				//419f
				//Get prereqs.
				$prereqs = Array();
				if ($hasPreReq)
				{
					//Check to see if we have coreqs, and set an appropriate
					//end point.
					$endPreReq = $hasCoReq ? $coreqStart : $ddEndPos;
				

					$startPreReq = strpos($cleanHtml, $code, $currentPos) + 5;
					while ($startPreReq > 5 && $startPreReq < $endPreReq)
					{
						$tmp = "";
						$val = $cleanHtml[$startPreReq++];
						while(ctype_alpha($val) || ctype_digit($val))
						{
							$tmp.=$val;
							$val = $cleanHtml[$startPreReq++];
						}

						//Store the pre-req course code.
						$prereqs[] = $tmp;
						//Update our ptr.
						$startPreReq = strpos($cleanHtml, $code, $startPreReq) + 5;
					}

					$currentPos = $endPreReq;
				}

				//Get coreqs.
				$coreqs = Array();
				if ($hasCoReq)
				{
					//Check to see if we have coreqs, and set an appropriate
					//end point.
					$endCoReq = $ddEndPos;
					
					$startCoReq = strpos($cleanHtml, $code, $currentPos) + 5;
					while ($startCoReq > 5 && $startCoReq < $endCoReq)
					{
						$tmp = "";
						$val = $cleanHtml[$startCoReq++];
						while(ctype_alpha($val) || ctype_digit($val))
						{
							$tmp.=$val;
							$val = $cleanHtml[$startCoReq++];
						}

						//Store the pre-req course code.
						$coreqs[] = $tmp;
						//Update our ptr.
						$startCoReq = strpos($cleanHtml, $code, $startCoReq) + 5;
					}

					$currentPos = $endCoReq;
				}

				$currentPos = $ddEndPos;

				//Now that we have all of this information, we can hunt down data from another calendar:
				//URL is like this: https://courses.students.ubc.ca/cs/main?pname=subjarea&tname=subjareas&req=5&dept=COSC&course=211&section=001
				//Replacing &course with a course name and section with 001 and ticking until it doesn't give a valid course name.
				//Finding hasLab comes from searching from L01 and if the failure pattern doesn't match the course has a lab.
				
				$stopSearch = false;
				$tick = 1;
				$teacher = "nobody";
				$numberFree = 0;
				$numberTotal = 0;
				while (!$stopSearch)
				{
				    
				    //Better code
				    if ($tick <= 9)
				    {
				      $searchSection = '00'.$tick;
				    }
				    else 
				    {
				      $searchSection = '0'.$tick;
				    }
				    
				    //Fetch.
				    $newUrl = 'https://courses.students.ubc.ca/cs/main?campuscd=UBCO&pname=subjarea&tname=subjareas&req=5&dept='.$code.'&course='.$courseNum.'&section='.$searchSection;
				    
				    //$req = new HTTP_Request2($newUrl, HTTP_Request2::METHOD_GET);
				    //$req = $req->send();
				    //$fileToSearch = $req->getBody();
				    try
				    {
				    	$fileToSearch = file_get_contents($newUrl);
				    }
				    catch(Exception $ex)
				    {
				    	$fileToSearch = "";
				    }
				
				    //Teacher time.  Need to add a certain length though- if we start getting weird characters we need to try something else though.
				    $tStart = strpos($fileToSearch, "<td>Instructor:  </td>");
				    
				    
				    //If this entry isn't there, end the loop immediately.
				   if ($tStart === false) 
				   {
				    $stopSearch = true;
				    break;
				   }
				   else {
				       //Magic number; it's the length of the link after the search.
				   $tStart += 214;
				   }
				   
				   
				   
				   
				   
				   
				   $tEnd = strpos($fileToSearch, "</a></td>");
				    
				    /* Testing block for teachers
				    echo $tStart;
				    echo "<br>";
				    echo $tEnd;
				    echo "<br>";
				    */
				    
				    //Does the course have one teacher or many?
				    $teacherComp = substr($fileToSearch, $tStart, $tEnd-$tStart);
				    if ($tick == 1 || $teacher == $teacherComp)
				    {
				      $teacher = $teacherComp;
				    }
				    else
				    {
				      $teacher = "Multiple";
				    }
				    
				    //Time to collect seats.
				    $sStart = strpos($fileToSearch, "Total Seats Remaining:</td><td align=left><strong>")+50;
				    //That last part is most important
				    $sEnd = strpos($fileToSearch, "</strong></td></tr><tr><td width=200px>Currently");
				    //And grab the number of seats left.
				    $numberFree = substr($fileToSearch, $sStart, $sEnd - $sStart);
				    
				    /*
				    echo $sStart;
				    echo "<br>";
				    echo $sEnd;
				    echo "<br>";
				    */
				    
				    //Do the same thing but get the number registered this time.
				    $rStart = strpos($fileToSearch, "Currently Registered:</td><td align=left><strong>")+49;
				    $rEnd = strpos($fileToSearch, "</strong></td></tr><tr><td width=200px>General");
				    
				    /*
				    echo $rStart;
				    echo "<br>";
				    echo $rEnd;
				    echo "<br>";
				    */
				    
				    $numberTotal += (substr($fileToSearch, $rStart, $rEnd - $rStart) + $numberFree);
				    
				    $tick++;
				}
				
				
				$labPresent = TRUE;
				//Does the section have a lab?
				$labUrl = 'https://courses.students.ubc.ca/cs/main?campuscd=UBCO&pname=subjarea&tname=subjareas&req=5&dept='.$code.'&course='.$courseNum.'&section=L01';
				
				//$req = new HTTP_Request2($labUrl, HTTP_Request2::METHOD_GET);
				//$req = $req->send();
				//$fileToCheck = $req->getBody();
				try
				{
					$fileToCheck = file_get_contents($labUrl);
				}
				catch(Exception $ex)
				{
					$fileToCheck = "";
				}
				$lCheck = strpos($fileToCheck, "<h4>");
				if ($lCheck == false) $labPresent = FALSE;
				
				//Decrement the ticker by one, it's the # of sections now
				//$section = $tick - 1;
				//The ticker is the number of sections.
				$section = $tick;
				
				/*
				echo $labPresent;
				echo "<br>";
				echo $teacher;
				echo "<br>";
				echo $section;
				echo "<br>";
				echo $stopSearch;
				echo "<br>";
				echo $numberTotal;
				echo "<br>";
				echo $numberFree;
				*/
				
				
				//Create a course object and store our data.
				$newCourse = new Course($courseName, $courseNum, $code, $program);
				$newCourse->description = trim($courseDesc);
				foreach ($prereqs as $req)
					$newCourse->add_required($req);
				foreach ($coreqs as $req)
					$newCourse->add_corequired($req);
					
					
				$newCourse->hasLab = $labPresent;
				$newCourse->sections = $section;
				$newCourse->teacher = $teacher;
				$newCourse->seatsOpen = $numberFree;
				$newCourse->seatsTotal = $numberTotal;

				$courses[] = $newCourse;

				//Move to next course.
				$currentPos = strpos($cleanHtml, "<dt>", $currentPos);
			} 

			return $courses;
		}


		/**
		 * Parses the course listing for a given program and returns a 
		 * JSON-encoded string containing all relevant course information.
		 *
		 * $code: The program code to get courses for, i.e. "COSC".
		 *
		 * return: A JSON-encoded string.
		 */
		public static function get_course_json($code, $refresh)
		{
			$db = new db();

			//Check to make sure the code is valid.
			if (strlen($code) > 4 || strlen($code) < 3)
				return "bad code";

			//Check to see if we've already cached the course listing in a 
			//flat file. If we have - load that. Unless we're forcing a refresh.
			if (file_exists($code.'.json') && $refresh != true)
			{
				return file_get_contents($code.'.json');
			}
			else
			{
				$courses = Parser::parse_courses(
						Parser::get_courses_html($code), $code);

				$data = json_encode($courses);

				//Write data out to a flat file. Poor man's db/cache.
				file_put_contents($code.'.json', $data);

				//Sends new file off to the database
				$db->uploadcourse($code,$data);
				$db->close();
				
				return $data;
			}
		}
	}

	header('Content-type: application/json');
	echo Parser::get_course_json($_GET['code'], isset($_GET['refresh']));
?>
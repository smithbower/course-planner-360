This project is a PHP webcrawler pulling from 2 different calendars to not only display courses and their mappings in relation to each other,
but also their current instructors, sections, and whether or not they are full.

This project is in 3 parts: one, a parser/webcrawler; two, a template for each course; and three, an HTML file that displays this data using D3.js.

The crawler records course names, numbers, pre/c-requisites, and descriptions from the main academic calendar.
Once it has those, it crawls another calendar for other information, including teacher (one or many), number of sections and seats total/available, and if the course has a lab.

As soon as it is finished (per-course), it makes an object and stores it on a database using JSON.

The main page uses D3 to display all of this information; retrieving it from prior-stored JSON or grabbing it from the site to both reduce server load and time it takes to generate the map of courses.


Calling the parser directly with the parameter code=COSC (for example; other codes like BIOL also work- see index.html for examples) will return the data we seek in JSON form.
The database (and by extension index.html) currently don't work properly; though the formatting is there, it just doesn't seem to return anything though seemingly the operation still actually completes.
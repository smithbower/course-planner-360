<?php
	/**
	 * course.php
	 *
	 * Container class that represents a course.
	 *
	 * A course is made up of a name, course number, description, optional
	 * prereqs with optional conditions, and optional coreqs with optional
	 * conditions.
	 */
?>
<?php
	class Course
	{
		public $name;			//String.
		public $number;			//String.
		public $description;		//String.
		public $required;		//Array.
		public $corequired;		//Array.
		public $year;			//Number.
		public $code;			//String.
		public $program;		//String.
		public $sections;		//Number.
		public $teacher;		//Array.
		public $hasLab;			//Boolean.
		public $seatsOpen;		//Number
		public $seatsTotal;		//Number

		/**
		 * Creates a new course.
		 *
		 * $name: The long-form name of the course.
		 * $number: The course number (i.e. 222).
		 */
		public function Course($name, $number, $code, $program)
		{
			$this->name = $name;
			$this->number = $number;
			$this->code = $code;
			$this->program = $program;

			switch (intval($number[0]))
			{
				case 1:
					$this->year = "one";
					break;

				case 2:
					$this->year = "two";
					break;

				case 3:
					$this->year = "three";
					break;

				case 4:
					$this->year = "four";
					break;

				case 5:
					$this->year = "five";
					break;

				case 6:
					$this->year = "six";
					break;

				default:
					$this->year = "unknown";
					break;
			}

			$this->description = "none";
			$this->required = Array();
			$this->corequired = Array();
			
			//And actually store the new stuff this time.
			$this->teacher = $teacher;
			$this->sections = $sections;
			$this->hasLab = $hasLab;
			$this->seatsOpen = $seatsOpen;
			$this->seatsTotal = $seatsTotal;
		}

		/**
		 * Add a required course to this course's listing.
		 *
		 * $courseNum: The course number for the required course (i.e. 222).
		 */
		public function add_required($courseNum)
		{
			//TODO: support conditions, one of, all of, etc.
			$this->required[] = $courseNum;
		}

		/**
		 * Add a corequired course to this course's listing.
		 *
		 * $courseNum: The course number for the corequired course (i.e. 222).
		 */
		public function add_corequired($courseNum)
		{
			//TODO: support conditions, one of, all of, etc.
			$this->corequired[] = $courseNum;
		}		
	}
?>